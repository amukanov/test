provider "aws" {
   region     = "eu-central-1"
}

resource "aws_elb" "elb" {
  name               = "terraform-elb"
  availability_zones = ["eu-central-1a", "eu-central-1b", "eu-central-1c"]
  security_groups = [aws_security_group.main.id]

  listener {
    instance_port     = 8080
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

/*
  listener {
    instance_port = 8080
    instance_protocol = "http"
    lb_port = 443
    lb_protocol = "https"
    ssl_certificate_id = "arn:aws:iam::123456789012:server-certificate/certName"
}
*/

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:8080/"
    interval            = 30
  }
  instances                   = [aws_instance.ubuntu_server.id]
  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400
  tags = {
    Name = "terraform-elb"
  }
}


resource "aws_instance" "ubuntu_server" {

    ami = "ami-065deacbcaac64cf2"
    instance_type = "t2.micro"
    key_name= "aws_key"
    vpc_security_group_ids = [aws_security_group.main.id]

}

resource "aws_security_group" "main" {
  egress    {

      cidr_blocks      = [ "0.0.0.0/0", ]
      from_port        = 0
      protocol         = "-1"
      to_port          = 0
    }

 ingress   {

     cidr_blocks      = [ "0.0.0.0/0", ]
     from_port        = 0
     protocol         = "-1"
     to_port          = 0
  }

}

resource "aws_key_pair" "deployer" {
  key_name   = "aws_key"
  public_key = "ssh-rsa **********"

  connection {
      type        = "ssh"
      host        = self.public_ip
      user        = "ubuntu"
            private_key = file("/path")
      timeout     = "4m"
   }

}


output "ec2_global_ips" {
  value = ["${aws_instance.ubuntu_server.*.public_ip}"]
}
